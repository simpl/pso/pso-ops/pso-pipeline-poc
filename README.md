# Program Support Office repositories

## Overview
Welcome to the **PSO Quality Validation Code Repository**. This repository contains the code used by the **Program Support Office (PSO)** to independently validate the quality of the **SIMPL programme**. As an autonomous entity, PSO ensures that the programme meets its quality standards, maintaining transparency and excellence.

This repository is publicly accessible to promote openness and allow stakeholders to understand how independent quality validation is performed within SIMPL. However, **this repository is not meant to be collaborative**; it is strictly for transparency purposes regarding the **Independent Validation and Verification (IVV)** activities conducted by PSO.

## Important Note
PSO repositories are **not part of the SIMPL codebase**. If you are looking for the SIMPL programme's source code or development repositories, please refer to the official [SIMPL repositories](https://code.europa.eu/simpl/simpl-open).


# PSO-PIPELINE-POC
This repository triggers the main PSO pipeline to test Simpl repositories. It clones the code, performs quality code validations, SAST, SCA and builds the container images.

TODO:
- Automatic deployments of PSO environments on new releases.
- Automatic DAST reports using [PSO DAST pipeline](https://code.europa.eu/simpl/pso/pso-ops/pso-dast).
- Automatic notifications with pipeline and deployment status to relevant stakeholders.